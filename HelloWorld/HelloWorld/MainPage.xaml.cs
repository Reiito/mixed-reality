﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Microsoft.OneDrive.Sdk;
using Windows.UI.Popups;
using System.Diagnostics;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloWorld
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        //ONEDRIVE API STUFF
        //======================================================
        private readonly string[] _scopes =
        {
            "onedrive.readwrite",
            "onedrive.appfolder",
            "wl.signin"
        };

        private IOneDriveClient _client;
        private string _savedId;
        private AccountSession _session;
        //======================================================

        public MainPage()
        {
            this.InitializeComponent();
        }

        //OneDrive FUNCTION
        //Sauce: https://msdn.microsoft.com/en-us/magazine/mt632271.aspx
        //Sauce: https://github.com/OneDrive/onedrive-sdk-csharp
        private async void OneDrive(object sender, RoutedEventArgs e)
        {
            Exception error = null;
            try
            {
                _session = null;

                // Using the OnlineIdAuthenticator
                _client = OneDriveClientExtensions.GetClientUsingOnlineIdAuthenticator(
                    _scopes);

                // Using the WebAuthenticationBroker
                //_client = OneDriveClientExtensions.GetClientUsingWebAuthenticationBroker(
                //    "000000004C172C3F",
                //    _scopes);

                _session = await _client.AuthenticateAsync();
                Debug.WriteLine($"Token: {_session.AccessToken}");

                var dialog = new MessageDialog("You are authenticated!", "Success!");
                await dialog.ShowAsync();
            }
            catch (Exception ex)
            {
                error = ex;
            }

            if (error != null)
            {
                var dialog = new MessageDialog("Problem when authenticating!", "Sorry!");
                await dialog.ShowAsync();
            }
        }




        // Launch the URI
        // BUTTON 2 (Microsoft Word)
        private async void MicrosoftWord(object sender, RoutedEventArgs e)
        {
            //URI Doc https://docs.microsoft.com/en-us/office/client-developer/office-uri-schemes
            //Sauce https://stackoverflow.com/questions/42217471/uri-scheme-ms-wordnftu-not-working-and-not-opening-word
            // The URI to launch
            string uriToLaunch = "ms-word:nft|u|https://omextemplates.content.office.net/support/templates/en-us/tf10067038.dotx";
            
            // Create a Uri object from a URI string 
            var uri = new Uri(uriToLaunch);
            // Launch the URI
            var success = await Windows.System.Launcher.LaunchUriAsync(uri);

            if (success)
            {
                // URI launched
            }
            else
            {
                // URI launch failed
            }
        }

        //Unity Collaborative Space
        private async void Unity(object sender, RoutedEventArgs e)
        {
            ContentDialog subscribeDialog = new ContentDialog
            {
                Title = "Unity Collaborative Space",
                Content = "Do you wish to launch the collaborative experience?",
                CloseButtonText = "No",
                PrimaryButtonText = "Yes",
            };

            ContentDialogResult result = await subscribeDialog.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                //launch unity app
            }
            else
            {
                //do nothing
            }
        }

        //CreateNewView
        private async void GoogleDrive(object sender, RoutedEventArgs e)
        {
            var appViewId = ApplicationView.GetForCurrentView().Id;

            //Create a new view
            CoreApplicationView newCoreAppView = CoreApplication.CreateNewView();

            await newCoreAppView.Dispatcher.RunAsync(
                CoreDispatcherPriority.Low, 
                () =>
                {
                    //Get the created windows
                    Window window = Window.Current;
                    ApplicationView newAppView = ApplicationView.GetForCurrentView();

                    //create a new frame and navigate to the page
                    var secondFrame = new Frame();
                    window.Content = secondFrame;
                    secondFrame.Navigate(typeof(SecondPage));

                    //activate the new Window
                    window.Activate();

                    //make the new window standalone
                    ApplicationViewSwitcher.TryShowAsStandaloneAsync(newAppView.Id,
                        ViewSizePreference.UseMore, appViewId, ViewSizePreference.Default);
                });
        }
    }
}
