﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using HoloToolkit.Unity.InputModule;

public class OwlBotManager : MonoBehaviour, IInputClickHandler
{

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
    }

    /// <summary>
    /// Handle when owl mesh is clicked
    /// </summary>
    /// <param name="eventData"></param>
    public void OnInputClicked(InputClickedEventData eventData)
    {
        GameManager.gameManagerInstance.GestureBehaviour();
        
    }
}
