﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolbeltManager : MonoBehaviour
{
    public float forwardDistance = 2f;
    public float belowDistance = 1.25f;

    public float speed = 2f;

    public float distanceThreshold = 0.2f;
    public float rotLimit = 0.2f;

    GameManager gameManager;

    float rotDiff;

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;
        PositionToolBelt();
        lastPos = transform.position;
    }

    Vector3 lastPos;
    private void Update()
    {
        // Follow user's position
        if (Vector3.Distance(lastPos, Camera.main.transform.position) > distanceThreshold)
        {
            lastPos = Camera.main.transform.position;
            PositionToolBelt();
        }

        // Rotate around the player with rotation space
        rotDiff = transform.rotation.y - Camera.main.transform.rotation.y;
        if (rotDiff < -rotLimit || rotDiff > rotLimit)
        {
            PositionToolBelt();
        }
    }

    /// <summary>
    /// Place toolbelt in front of user
    /// </summary>
    void PositionToolBelt()
    {
        transform.position = Vector3.Lerp(transform.position, Camera.main.transform.position + Camera.main.transform.forward * forwardDistance, Time.deltaTime * speed);
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, Camera.main.transform.position.y - belowDistance, transform.position.z), Time.deltaTime * speed);
        transform.LookAt(2 * transform.position - Camera.main.transform.position);
    }

#region Buttons

    public void OnModeChangePress(int newMode)
    {
        gameManager.mode = (InteractionMode)newMode;
        CursorManager.cursorManagerInstance.SwitchCursor(newMode);
    }

    public void OnLayerUpPress()
    {
        if (gameManager.CurrentMesh)
        {
            gameManager.CurrentLayerIndex++;
            gameManager.SwapLayer();
        }
    }

    public void OnLayerDownPress()
    {
        if (gameManager.CurrentMesh)
        {
            gameManager.CurrentLayerIndex--;
            gameManager.SwapLayer();
        }
    }

#endregion

}
