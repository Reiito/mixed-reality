﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public static CursorManager cursorManagerInstance; 

    public GameObject[] cursorObjects;

    GameObject currentCursor;

    private void Awake()
    {
        cursorManagerInstance = this;

        currentCursor = cursorObjects[0];

        foreach(GameObject go in cursorObjects)
        {
            go.SetActive(false);
        }
    }

    /// <summary>
    /// Swap to new cursor mesh depending on current mode
    /// </summary>
    /// <param name="newMode"></param>
    public void SwitchCursor(int newMode)
    {
        if (currentCursor != cursorObjects[newMode])
        {
            currentCursor.SetActive(false);
            currentCursor = cursorObjects[newMode];
            currentCursor.SetActive(true);
        }
    }
}
