﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class DictationManager : MonoBehaviour, IDictationHandler
{
    /// <summary>
    /// WIP: recording dictation for notation input using the microphone

    bool isRecording;
    public void ToggleRecording()
    {
        if (isRecording)
        {
            isRecording = false;
            StartCoroutine(DictationInputManager.StopRecording());
        }
        else
        {
            isRecording = true;
            StartCoroutine(DictationInputManager.StartRecording(null, 5f, 20f, 10));
        }
    }

    public void OnDictationHypothesis(DictationEventData eventData)
    {
        //optionsMenu.SetCommentText(eventData.DictationResult);
    }

    public void OnDictationResult(DictationEventData eventData)
    {
        //optionsMenu.SetCommentText(eventData.DictationResult);
    }

    public void OnDictationComplete(DictationEventData eventData)
    {
        //optionsMenu.SetCommentText(eventData.DictationResult);
        //optionsMenu.HideCommentInput();
        //ToggleRecording();
    }

    public void OnDictationError(DictationEventData eventData)
    {
        //pinComment = "";
        //optionsMenu.HideCommentInput();
        ToggleRecording();
    }

    /// </summary>


}