﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using HoloToolkit.Unity.InputModule;

public class MeshManager : MonoBehaviour, IInputClickHandler, IManipulationHandler, INavigationHandler
{

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.gameManagerInstance;
    }

    /// <summary>
    /// Handle mesh when clicked
    /// </summary>
    /// <param name="eventData"></param>
    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (gameManager.mode == InteractionMode.PIN)
        {
            gameManager.GestureBehaviour();
        }
    }

    // Both reset when let go

    // Handle movement and scale interactions
    #region Movement & Scale

    Vector3 manipulationOriginalPosition = Vector3.zero;
    public void OnManipulationStarted(ManipulationEventData eventData)
    {
        gameManager.CanUpdate = false;

        if (gameManager.mode == InteractionMode.MOVE)
        {
            InputManager.Instance.PushModalInputHandler(gameObject);
            manipulationOriginalPosition = transform.position;
        }
        else if (gameManager.mode == InteractionMode.SCALE)
        {
            InputManager.Instance.PushModalInputHandler(gameObject);
            manipulationOriginalPosition = transform.localScale;
        }
    }

    public void OnManipulationUpdated(ManipulationEventData eventData)
    {
        if (gameManager.mode == InteractionMode.MOVE)
        {
            transform.position = manipulationOriginalPosition + eventData.CumulativeDelta;
        }
        else if (gameManager.mode == InteractionMode.SCALE)
        {
            transform.localScale += (manipulationOriginalPosition * eventData.CumulativeDelta.y) / gameManager.scaleSensitivity;
            transform.localScale = new Vector3(cClamp(transform.localScale.x), cClamp(transform.localScale.y), cClamp(transform.localScale.z));
        }
    }

    public void OnManipulationCompleted(ManipulationEventData eventData)
    {
        if (gameManager.mode == InteractionMode.MOVE)
        {
            transform.position = manipulationOriginalPosition;
        }
        else if (gameManager.mode == InteractionMode.SCALE)
        {
            transform.localScale = manipulationOriginalPosition;
        }

        InputManager.Instance.PopModalInputHandler();

        gameManager.CanUpdate = true;
    }

    public void OnManipulationCanceled(ManipulationEventData eventData)
    {
        if (gameManager.mode == InteractionMode.MOVE)
        {
            transform.position = manipulationOriginalPosition;
        }
        else if (gameManager.mode == InteractionMode.SCALE)
        {
            transform.localScale = manipulationOriginalPosition;
        }

        InputManager.Instance.PopModalInputHandler();

        gameManager.CanUpdate = true;
    }

    #endregion

    // Handle rotation interaction
    #region Rotation

    Quaternion navigationOriginalRotation = Quaternion.identity;
    public void OnNavigationStarted(NavigationEventData eventData)
    {
        gameManager.CanUpdate = false;

        navigationOriginalRotation = transform.rotation;

        InputManager.Instance.PushModalInputHandler(gameObject);
    }

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        if (gameManager.mode == InteractionMode.ROTATE)
        {
            float rotationFactorX = eventData.NormalizedOffset.x * gameManager.rotationSensitivity;
            transform.Rotate(new Vector3(0, -1 * rotationFactorX, 0));
        }
    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {
        transform.rotation = navigationOriginalRotation;

        InputManager.Instance.PopModalInputHandler();

        gameManager.CanUpdate = true;
    }

    public void OnNavigationCanceled(NavigationEventData eventData)
    {
        transform.rotation = navigationOriginalRotation;

        InputManager.Instance.PopModalInputHandler();

        gameManager.CanUpdate = true;
    }

    float cClamp(float value)
    {
        return Mathf.Clamp(value, 0.25f, 1f);
    }

    #endregion
}
