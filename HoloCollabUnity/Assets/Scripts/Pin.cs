﻿using UnityEngine;

public class Pin : MonoBehaviour
{
    public MeshRenderer pinHeadMesh;
    public TextMesh commentMesh;
    public Material[] pinMaterials;

    PinDetails pinDetails;
    public PinDetails PinDetails { get { return pinDetails; } set { pinDetails = value; } }

    private void Awake()
    {
        pinHeadMesh.material = pinMaterials[0];
    }

    public string GetComment()
    {
        return commentMesh.text;
    }

    /// <summary>
    /// Update text mesh with given annotation
    /// </summary>
    /// <param name="comment"></param>
    public void AddComment(string comment)
    {

        if (comment == null || comment == "")
        {
            GameManager.gameManagerInstance.CanUpdate = true;
            return;
        }

        pinDetails.comment = comment;

        commentMesh.text = comment;
        commentMesh.gameObject.SetActive(true);

        pinDetails.display = true;

        pinHeadMesh.material = pinMaterials[1];
    }

    public void RemoveComment()
    {

        commentMesh.text = "";
        commentMesh.gameObject.SetActive(false);
        
        pinHeadMesh.material = pinMaterials[0];
    }

    /// <summary>
    /// Turn pin annotations on or off
    /// </summary>
    public void ToggleCommentMesh()
    {

        if (commentMesh.gameObject.activeInHierarchy)
        {
            pinDetails.display = false;
            commentMesh.gameObject.SetActive(false);
        }
        else
        {
            pinDetails.display = true;
            commentMesh.gameObject.SetActive(true);
        }
    }

    public bool HasComment()
    {
        if (commentMesh.text != "" || !commentMesh)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetGlobalScale(float globalScale)
    {
        transform.localScale = Vector3.one;
        transform.localScale = new Vector3(globalScale / transform.lossyScale.x, globalScale / transform.lossyScale.y, globalScale / transform.lossyScale.z);
    }

    public void SetGlobalScale(Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
    }
}
